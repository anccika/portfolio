// JavaScript Document

function toggleMyProjectsNav(){
	var myProjectsNav = document.getElementById("myProjectsNav");
	var myProjectsTab = document.getElementById("myProjects");
	if (myProjectsNav.style.visibility === "visible") {
		myProjectsNav.style.visibility = "hidden";
		myProjectsTab.style.backgroundColor = "#BFBFBF";
	} else {
		myProjectsNav.style.visibility = "visible";
		myProjectsTab.style.backgroundColor = "white";
	}
}

function toggleMobileMenu(){
	var mobileMenuContent = document.getElementById("mobileMenuContent");
	if (mobileMenuContent.style.visibility === "visible") {
		mobileMenuContent.style.visibility = "hidden";	
	} else {
		mobileMenuContent.style.visibility = "visible";
	}
}
